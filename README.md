## Data description

- FNO1d Burgers Equation

  [burgers_data_R10.mat](https://drive.google.com/drive/folders/1UnbQh2WWc6knEHbLn-ZaXrKUZhp7pjt-?usp=sharing)
  with field 'a' 2048\*8192 and 'u' 2048\*8192

  use the first 1000 samples for training and the last 100 ones for testing

- FNO2d Darcy Flow

  [piececonst_r241_N1024_smooth1.mat](https://drive.google.com/drive/folders/1UnbQh2WWc6knEHbLn-ZaXrKUZhp7pjt-?usp=sharing)
  with filed 'coeff' 1024\*241\*241 and 'sol' 1024\*241\*241

  [piececonst_r241_N1024_smooth2.mat](https://drive.google.com/drive/folders/1UnbQh2WWc6knEHbLn-ZaXrKUZhp7pjt-?usp=sharing)
  with filed 'coeff' 1024\*241\*241 and 'sol' 1024\*241\*241

  use the first mat for training and the second one for testing

- FNO2dtime/FNO3d Navier Stokes Equation

  [NavierStokes_V1e-5_N1200_T20.mat](https://drive.google.com/drive/folders/1UnbQh2WWc6knEHbLn-ZaXrKUZhp7pjt-?usp=sharing)
  with filed 'u' 1200\*64\*64\*20

  use the first 1000 samples for training and the last 100 ones for testing

- PINO2d Darcy Flow

  [piececonst_r421_N1024_smooth1.mat](https://hkzdata.s3.us-west-2.amazonaws.com/PINO/piececonst_r421_N1024_smooth1.mat)
  with filed 'coeff' 1024\*421\*421 and 'sol' 1024\*421\*421

  [piececonst_r421_N1024_smooth2.mat](https://hkzdata.s3.us-west-2.amazonaws.com/PINO/piececonst_r421_N1024_smooth2.mat)
  with filed 'coeff' 1024\*421\*421 and 'sol' 1024\*421\*421

- PINO3d Navier Stokes Equation

  [NS_fft_Re500_T4000.npy](https://hkzdata.s3.us-west-2.amazonaws.com/PINO/NS_fft_Re500_T4000.npy)
  with data size 4000\*65\*64\*64 for training

  [NS_fine_Re500_T128_part2.npy](https://hkzdata.s3.us-west-2.amazonaws.com/PINO/NS_fine_Re500_T128_part2.npy)
  with data size 100\*129\*128\*128 for testing

## Train and Eval

```bash
git clone https://gitee.com/burning489/mindx-science.git
cd mindx-science/neuralop
git clone https://gitee.com/burning489/neuralop_torch.git torch
```

### FNO
To train MindSpore FNO models, use, e.g.,
```bash 
python _fno_ex/train_fno.py --config_path configs/fno1d_pretrain.yaml
python _fno_ex/train_fno.py --config_path configs/fno2d_pretrain.yaml
python _fno_ex/train_fno.py --config_path configs/fno2dtime_pretrain.yaml
python _fno_ex/train_fno.py --config_path configs/fno3d_pretrain.yaml
```

To eval MindSpore FNO models, use, e.g.,
```bash 
python _fno_ex/eval_fno.py --config_path configs/fno1d_pretrain.yaml
python _fno_ex/eval_fno.py --config_path configs/fno2d_pretrain.yaml
python _fno_ex/eval_fno.py --config_path configs/fno2dtime_pretrain.yaml
python _fno_ex/eval_fno.py --config_path configs/fno3d_pretrain.yaml
```

To train and eval PyTorch FNO models, use, e.g.,
```bash 
python torch/fno1d.py --config_path configs/fno1d_pretrain.yaml
python torch/fno2d.py --config_path configs/fno2d_pretrain.yaml
python torch/fno2dtime.py --config_path configs/fno2dtime_pretrain.yaml
python torch/fno3d.py --config_path configs/fno3d_pretrain.yaml
```

To profile MindSpore FNO models, use, e.g.,
```bash 
python _fno_ex/train_fno.py --config_path configs/fno1d_prof.yaml
python _fno_ex/train_fno.py --config_path configs/fno2d_prof.yaml
python _fno_ex/train_fno.py --config_path configs/fno2dtime_prof.yaml
python _fno_ex/train_fno.py --config_path configs/fno3d_prof.yaml
```

To profile PyTorch FNO models, use, e.g.,
```bash 
python torch/fno1d.py --config_path configs/fno1d_prof.yaml
python torch/fno2d.py --config_path configs/fno2d_prof.yaml
python torch/fno2dtime.py --config_path configs/fno2dtime_prof.yaml
python torch/fno3d.py --config_path configs/fno3d_prof.yaml
```

### PINO
To train MindSpore PINO models, use, e.g.,
```bash 
python _pino_ex/train_pino.py --config_path configs/pino2d_pretrain.yaml
python _pino_ex/train_pino.py --config_path configs/pino3d_pretrain.yaml
```

To eval MindSpore PINO models, use, e.g.,
```bash 
python _pino_ex/eval_pino.py --config_path configs/pino2d_pretrain.yaml
python _pino_ex/eval_pino.py --config_path configs/pino3d_eval.yaml
```

To train and eval PyTorch PINO models, use, e.g.,
```bash 
python torch/pino2d.py --config_path configs/pino2d_pretrain.yaml
python torch/pino3d.py --pretrain_path configs/pino3d_pretrain.yaml --eval_path configs/pino3d_eval.yaml
```

To profile MindSpore PINO models, use, e.g.,
```bash 
python _pino_ex/train_pino.py --config_path configs/pino2d_prof.yaml
python _pino_ex/train_pino.py --config_path configs/pino3d_prof.yaml
```

To profile PyTorch PINO models, use, e.g.,
```bash 
python torch/pino2d.py --config_path configs/pino2d_prof.yaml
python torch/pino3d.py --pretrain_path configs/pino3d_prof.yaml --eval_path configs/pino3d_eval.yaml
```