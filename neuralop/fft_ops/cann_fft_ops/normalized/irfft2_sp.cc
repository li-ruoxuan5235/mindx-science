/**
 * Copyright 2021 Huawei Technologies Co., Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "irfft2_sp.h"

#include <iostream>
#include <algorithm>
#include <complex>
#include <map>
#include <vector>
#include <Eigen/Dense>
#include <unsupported/Eigen/CXX11/Tensor>
#include <unsupported/Eigen/FFT>
#include "cpu_kernel_utils.h"
#include "cpu_types.h"
#include "log.h"
#include "status.h"
#include "utils/kernel_util.h"
#define N2 2

using namespace std;
using namespace Eigen;

namespace {
const char *kIRFFT2_SP = "IRFFT2_SP";
const uint32_t kOutputNum = 1;
const uint32_t kInputNum = 4;
}  // namespace

namespace aicpu {
uint32_t IRFFT2SPCpuKernel::Compute(CpuKernelContext &ctx) {
  KERNEL_HANDLE_ERROR(NormalCheck(ctx, kInputNum, kOutputNum),
                      "[%s] check input and output failed.", kIRFFT2_SP);
  vector<int64_t> shape_x = ctx.Input(0)->GetTensorShape()->GetDimSizes();
  KERNEL_CHECK_FALSE((shape_x.size() >= N2), KERNEL_STATUS_PARAM_INVALID,
                     "Input must be at least rank 2, got [%zu].",
                     shape_x.size())
  DataType data_type = ctx.Input(0)->GetDataType();
  switch (data_type) {
    case DT_FLOAT:
      return IRFFT2SPCompute<float, complex<float>>(ctx);
    case DT_DOUBLE:
      return IRFFT2SPCompute<double, complex<double>>(ctx);
    default:
      KERNEL_LOG_ERROR("IRFFT2_SP kernel data type [%s] not support.",
                       DTypeStr(data_type).c_str());
      return KERNEL_STATUS_PARAM_INVALID;
  }
  return KERNEL_STATUS_OK;
}

template <typename T, typename C>
uint32_t IRFFT2SPComputeSingleBatch(CpuKernelContext &ctx, int64_t batch_idx) {
  const int64_t FFTRank = 2;
  vector<int64_t> s = ctx.GetAttr("s")->GetListInt();
  vector<int64_t> origin = ctx.GetAttr("origin")->GetListInt();
  vector<int64_t> shape_x = ctx.Input(0)->GetTensorShape()->GetDimSizes();
  vector<int64_t> shape_y = ctx.Output(0)->GetTensorShape()->GetDimSizes();
  int64_t mode1 = shape_x.at(shape_x.size() - 2);
  int64_t mode2 = shape_x.at(shape_x.size() - 1);
  int64_t dim1_y = shape_y.at(shape_y.size() - 2);
  int64_t dim2_y = shape_y.at(shape_y.size() - 1);
  int64_t num_x_batch = mode1 * mode2;
  int64_t num_y_batch = dim1_y * dim2_y;
  auto x1_re_ptr = reinterpret_cast<T *>(ctx.Input(0)->GetData()) + batch_idx * num_x_batch;
  auto x2_re_ptr = reinterpret_cast<T *>(ctx.Input(1)->GetData()) + batch_idx * num_x_batch;
  auto x1_im_ptr = reinterpret_cast<T *>(ctx.Input(2)->GetData()) + batch_idx * num_x_batch;
  auto x2_im_ptr = reinterpret_cast<T *>(ctx.Input(3)->GetData()) + batch_idx * num_x_batch;
  auto y_ptr = reinterpret_cast<T *>(ctx.Output(0)->GetData()) + batch_idx * num_y_batch;
  TensorMap<Eigen::Tensor<T, N2, RowMajor>> x1_re_tensor(x1_re_ptr, mode1, mode2);
  TensorMap<Eigen::Tensor<T, N2, RowMajor>> x2_re_tensor(x2_re_ptr, mode1, mode2);
  TensorMap<Eigen::Tensor<T, N2, RowMajor>> x1_im_tensor(x1_im_ptr, mode1, mode2);
  TensorMap<Eigen::Tensor<T, N2, RowMajor>> x2_im_tensor(x2_im_ptr, mode1, mode2);
  Eigen::Tensor<C, N2, RowMajor> x1_tensor(mode1, mode2);
  Eigen::Tensor<C, N2, RowMajor> x2_tensor(mode1, mode2);
  for (int64_t i = 0; i < num_x_batch; i++) {
    x1_tensor.data()[i].real(x1_re_tensor.data()[i]);
    x1_tensor.data()[i].imag(x1_im_tensor.data()[i]);
    x2_tensor.data()[i].real(x2_re_tensor.data()[i]);
    x2_tensor.data()[i].imag(x2_im_tensor.data()[i]);
  }
  Eigen::Tensor<C, N2, RowMajor> x_tensor(origin[0], origin[1]);
  x_tensor.setConstant(0);
  DSizes<DenseIndex, FFTRank> modes_start1{ 0, 0 };
  DSizes<DenseIndex, FFTRank> modes_start2{ origin[0] - mode1, 0 };
  DSizes<DenseIndex, FFTRank> modes_sizes{ mode1, mode2 };
  x_tensor.slice(modes_start1, modes_sizes) = x1_tensor;
  x_tensor.slice(modes_start2, modes_sizes) = x2_tensor;
  TensorMap<Eigen::Tensor<T, N2, RowMajor>> y_tensor(y_ptr, dim1_y, dim2_y);
  // Calculate the shape of the temporary tensor for the full FFT and the
  // region we will slice from input given fft_shape. We slice input to
  // fft_shape on its inner-most dimensions, except the last (which we
  // slice to fft_shape[-1] / 2 + 1).
  DSizes<DenseIndex, FFTRank> input_slice_sizes{ s[0], s[1] / N2 + 1 };
  Eigen::Tensor<C, N2, RowMajor> full_fft(s[0], s[1]);
  // Calculate the starting point and range of the source of
  // negative frequency part.
  auto neg_sizes = input_slice_sizes;
  neg_sizes[FFTRank - 1] = s[FFTRank - 1] - input_slice_sizes[FFTRank - 1];
  DSizes<DenseIndex, FFTRank> neg_target_indices;
  neg_target_indices[FFTRank - 1] = input_slice_sizes[FFTRank - 1];
  const DSizes<DenseIndex, FFTRank> start_indices;
  DSizes<DenseIndex, FFTRank> neg_start_indices;
  neg_start_indices[FFTRank - 1] = 1;
  full_fft.slice(start_indices, input_slice_sizes) =
      x_tensor.slice(start_indices, input_slice_sizes);
  // First, conduct IFFTs on outer dimensions. We save computation (and
  // avoid touching uninitialized memory) by slicing full_fft to the
  // subregion we wrote input to.
  if (FFTRank > 1) {
    const auto outer_axes = ArrayXi::LinSpaced(FFTRank - 1, 0, FFTRank - 2);
    full_fft.slice(start_indices, input_slice_sizes) =
        full_fft.slice(start_indices, input_slice_sizes)
            .template fft<BothParts, FFT_REVERSE>(outer_axes);
  }
  // Reconstruct the full FFT by appending reversed and conjugated
  // spectrum as the negative frequency part.
  Eigen::array<bool, FFTRank> reverse_last_axis;
  for (auto i = 0; i < FFTRank; i++) {
    reverse_last_axis[i] = (i == (FFTRank - 1));
  }
  if (neg_sizes[FFTRank - 1] != 0) {
    full_fft.slice(neg_target_indices, neg_sizes) =
        full_fft.slice(neg_start_indices, neg_sizes).reverse(reverse_last_axis).conjugate();
  }
  auto inner_axis = Eigen::array<int, 1>{ FFTRank - 1 };
  y_tensor = full_fft.template fft<RealPart, FFT_REVERSE>(inner_axis);
  return KERNEL_STATUS_OK;
}

template <typename T, typename C>
uint32_t IRFFT2SPCpuKernel::IRFFT2SPCompute(CpuKernelContext &ctx) {
  const int64_t FFTRank = 2;
  vector<int64_t> shape_x = ctx.Input(0)->GetTensorShape()->GetDimSizes();
  int64_t dim0 = 1;
  for (size_t i = 0; i < shape_x.size() - FFTRank; i++) { dim0 *= shape_x.at(i); }
  uint32_t min_core = 1;
  uint32_t max_core = std::max(min_core, aicpu::CpuKernelUtils::GetCPUNum(ctx) - 2);
  max_core = min(max_core, (uint32_t)dim0);
  auto shard_irfft2_sp = [&](int64_t start, int64_t end) {
    for (int64_t batch_idx = start; batch_idx < end; batch_idx++) {
      IRFFT2SPComputeSingleBatch<T, C>(ctx, batch_idx);
    }
  };
  KERNEL_HANDLE_ERROR(CpuKernelUtils::ParallelFor(ctx, dim0, dim0 / max_core, shard_irfft2_sp),
                      "IRFFT2_SP Compute failed.")
  return KERNEL_STATUS_OK;
}

REGISTER_CPU_KERNEL(kIRFFT2_SP, IRFFT2SPCpuKernel);
}  // namespace aicpu
